My collection of addons for [Premake](https://bitbucket.org/premake/premake-dev/wiki/Home)

* **Qt** : adds support for Qt (custom rules to build .ui, moc'able files, etc.)
* **CompilationUnit** : adds support for a compilation speed up technique derived from the [Single Compilation Unit](https://en.wikipedia.org/wiki/Single_Compilation_Unit) technique

You can browse the sources, there are Readmes' in each addon's folder explaining how to use them. If you have any question not addressed in those readmes, or have a feature request, a bug to report, etc. please feel free to feel an [Issue](https://bitbucket.org/dcourtois/premake-addons/issues?status=new&status=open)